# My APP

* Version: 0.0.1

## Description

こんなん考えてます。

仕事の合間にちまちまやってるよ。

## Use Plugin

* Bootstrap3 
* [sb-admin2](http://ironsummitmedia.github.io/startbootstrap-sb-admin-2/pages/index.html)
* [Bootstrap-select](https://silviomoreto.github.io/bootstrap-select/)
* Bootstrap-Markdown
* jQuery
* jQuery.Fullcalendar v2
* jQuery.MMenu
* [jQuery.timepicker](http://jonthornton.github.io/jquery-timepicker/)
* [select2](https://select2.github.io/)
* [marked.js](https://github.com/chjj/marked)
* [DataTable.Bootstrap](https://datatables.net/examples/styling/bootstrap.html)

## Development Team

* Kazuaki Sato - Project Manager, Director, Developer, Coder, more... (ugo .inc)

## Alumni

none.