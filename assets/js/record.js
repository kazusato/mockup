$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

/* PC以外のselect用 */
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
    $('.selectpicker').selectpicker('mobile');
}

/**
* ウィンドウリサイズ
*/
var resize = function(name)
{
	var w      = $(window).width() * 1.05;
    var height = 820;

    switch(name)
    {
    	case 'month' :
            height = resizeMonth(w);
    	break;
        case 'agendaWeek' :
            height = resizeWeek(w);

        break;
        case 'agendaDay' :
            height = resizeDay(w);
        break;
    }
    $('#calendar').fullCalendar('option', 'contentHeight', height);
}

/**
* 日付のリサイズ
*/
var resizeDay = function(w)
{
	if      (w > 860)             { return 820; }
    else if (w > 600 && w <= 860) { return 460; }
    else if (w > 360 && w <= 450) { return 460; }
    else if (w <= 360)            { return 460; }
}

/**
* 週のリサイズ
* とりあえずDayと同じ
*/
var resizeWeek = function(w)
{
	return resizeDay(w);
}

/**
* 月のリサイズ
*/
var resizeMonth = function(w)
{
	if      (w > 860)             { return 820; }
    else if (w > 600 && w <= 860) { return 460; }
    else if (w > 360 && w <= 450) { return 370; }
    else if (w <= 360)            { return 370; }
}

var adjustWeek = function(w)
{
    if(w <= 360)
    {
    	$('#calendar').fullCalendar('option', 'columnFormat', 'D');
    }
}

$(document).ready(function() {

    // page is now ready, initialize the calendar...

$('#calendar').fullCalendar({
    header : {
 		left:   'prev,next, today',
    	center: 'title',
    	right:  ''
	},
    /*
	views: {
		agendaWeek: {
			//titleFormat: 'yyyy年M月d日{ ～ }{[yyyy年]}{[M月]d日}',
			columnFormat: 'D',//'MM/D ddd'
		},
        agendaDay: {
            titleFormat: 'YYYY年MM月DD日（ddd）',
            columnFormat: 'dddd'
        },
    },
    */
    aspectRatio:2,
	defaultView : 'month',
    slotLabelFormat: 'H:mm',
    scrollTime: '09:00:00',
    minTime: '06:00:00',
    maxTime: '30:00:00',
    selectable: true,
    //selectHelper: true,
    timeFormat:'H:mm',
	viewRender : function(view) {
        //ヘッダを日付に書き換え
        //$('#page-header').text(view.title);
console.log(view);
         resize(view.name);
	},
    eventSources: 
    {
        events: [
        {
            title: 'Event1',
            start: '2015-11-10 10:00',
            end: '2015-11-10 12:00',
            editable : true,
            durationEditable: true,
            overlap: true
        },
        {
            title: 'Event2',
            start: '2015-11-10',
            editable : true,
            durationEditable: true,
            overlap: true
        }],
        
	},
	select: function(s, e, event, view){
        console.log(s);
        console.log(e);
        console.log(event);
        console.log(view);
        console.log(this);
	}

});

//Override the style of calendar button in Bootstrap
var today_btn = $('.fc-today-button');
$('#calendar button[type="button"]').removeClass().addClass('btn btn-outline btn-default');
$(today_btn).addClass('fc-today-button');

});